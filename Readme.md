LeasePlan
-- 


1.Introduction:
- 
- Tests are written using a combination of SerenityBDD, RestAssured, Cucumber, Junit & Maven.

2.Framework:
- 
- API calls & validations are made using RestAssured and SerenityRest which is a wrapper on top of RestAssured.
- Tests are written in BDD Gherkin format in Cucumber feature files, and it is represented as a living documentation in the test report.
- Each domain package consist of an Action class where API actions are defined.
- These domain models are called from a step-definitions class which are in-turn called from BDD tests.

3.Executing tests
- 

- Run mvn clean verify from the command line.
- The test results will be recorded here target/site/serenity/index.html.
- Run the below command from root directory to open the result after execution.
- open target/site/serenity/index.html

4.Update\Modifications of the framework
-

- groupId and artifactId modified
- Removed gradle setting 
- Modifications in pox.xml
- Serenity upgrade
- Restructured code



