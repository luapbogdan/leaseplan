Feature: Search for the products

  Scenario Outline:
    When the user calls get search test product endpoint <product>
    Then verify the search results of product should be displayed
    And verify the product <product> should be in Search results
    Examples:
      | product |
      | apple   |
      | mango   |
      | tofu    |
      | water   |

  @negative
  Scenario: Search test with unavailable product tea
    When the user calls get search test product endpoint car
    Then verify not found error should be displayed in search results

  @negative
  Scenario: Search test without product
    When the user calls get search test endpoint
    Then verify unauthorized error should be displayed in search result
