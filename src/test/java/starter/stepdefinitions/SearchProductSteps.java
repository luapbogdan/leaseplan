package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.actions.BasicSteps;
import starter.actions.SearchProductActions;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.*;


public class SearchProductSteps {

    @Steps
    public BasicSteps basicSteps;

    @Steps
    public SearchProductActions searchProductActions;

    @When("the user calls get search test product endpoint (.*)$")
    public void iCallTheGetSearchProductEndpoint(String product) {
        searchProductActions.searchTestProducts(product);
    }

    @When("the user calls get search test endpoint")
    public void iCallTheGetSearchTestEndpoint() {
        searchProductActions.searchTest();
    }

    @Then("verify the search results of product should be displayed")
    public void theSearchResultsOfProductShouldBeDisplayed(){
        basicSteps.responseCodeIs(200);
    }

    @Then("verify the product list should not be empty in Search results")
    public void theProductShouldBeDisplayedInSearchResults() {
        basicSteps.responseShouldNotBeEmptyList();
    }

    @Then("^verify the product (.*) should be in Search results$")
    public void theProductShouldBedInSearchResults(String product) {
        searchProductActions.verityProductInResponseResult(product);
    }

    @Then("verify not found error should be displayed in search results")
    public void notFoundErrorShouldBeDisplayedInSearchResult() {
        then().statusCode(404).body("detail.error", is(true));
    }

    @Then("verify unauthorized error should be displayed in search result")
    public void unauthorizedErrorShouldBeDisplayedInSearchResult() {
        then().statusCode(401).body("detail", is("Not authenticated"));
    }

}
