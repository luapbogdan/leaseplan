package starter.actions;

import io.cucumber.java.en.Then;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.assertj.core.api.Assertions.assertThat;

public class BasicSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(BasicSteps.class);

    public void responseCodeIs(int responseCode) {
        then().statusCode(responseCode);
    }

    public void responseShouldNotBeEmptyList() {
        List<HashMap<String, Object>> res = lastResponse().getBody().jsonPath().getList("$");
        LOGGER.info("Response list size is {}", res.size());
        assertThat(res.size()).isNotZero();
    }

}
