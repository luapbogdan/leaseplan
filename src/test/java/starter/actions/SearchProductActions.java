package starter.actions;

import io.cucumber.java.en.Then;
import net.serenitybdd.rest.SerenityRest;
import starter.common.RequestSpec;

import java.util.HashMap;
import java.util.List;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static org.assertj.core.api.Assertions.assertThat;

public class SearchProductActions {

    RequestSpec requestSpec;

    public void searchTestProducts(String product) {
        SerenityRest.given().log().uri().spec(RequestSpec.searchReqSpec()).pathParam("product", product).get("v1/search/test/{product}");
        System.out.println(SerenityRest.given().log().uri().spec(RequestSpec.searchReqSpec()).pathParam("product", product).get("v1/search/test/{product}"));
    }

    public void searchTest() {
        SerenityRest.given().log().uri().spec(RequestSpec.searchReqSpec()).get("v1/search/test");
        System.out.println(SerenityRest.given().log().uri().spec(RequestSpec.searchReqSpec()).get("v1/search/test"));
    }

    public void verityProductInResponseResult(String pro){
        List<HashMap<String, Object>> products = lastResponse().jsonPath().getList("$");
        assertThat(products).anyMatch(product -> product.get("title").toString().contains(pro));
    }

}
